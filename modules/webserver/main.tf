# Создаем Security Group
resource "aws_default_security_group" "myapp-sg" {
  vpc_id = var.vpc_id

  #   Открываем порты и определяем кто может к ним коннектится
  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 8080
    protocol    = "tcp"
    to_port     = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Открываем порты для отправления запросов во внешний мир, для того чтобы можно было устанавливать что-то например
  egress {
    from_port       = 0
    protocol        = "-1"
    to_port         = 0
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name : "${var.env_prefix}-sg"
  }
}


# Получаем последний образ
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  # сюда пишем владельца образа
  owners      = ["amazon"]
  # фильтры по которым искать образы
  filter {
    name   = "name"
    values = [var.image_name]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# проверим что выдаст нам в Data. Находит ли оно последний образ
#output "aws_ami" {
#  value = data.aws_ami.latest-amazon-linux-image
#}

# Ключ можно создать руками а можно и ресурсом. Тут мы создали key_pair на aws со своим публичным ключом
# и при подключении тогда нужно будет указывать приватный ключ, который привязан к публичному
#resource "aws_key_pair" "ssh-key" {
#  key_name   = "server-key"
#  # мой public key
#  public_key = file(var.public_key_location)
#}

# Создаем ec2
resource "aws_instance" "myapp-server" {
  # Amazon Machine Image это id операционки которую ты устанавливаешь на instance. Взял на aws морде
  #  ami = "ami-065deacbcaac64cf2"
  ami                         = data.aws_ami.latest-amazon-linux-image.id
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id // получаем из output другого модуля
  vpc_security_group_ids      = [aws_default_security_group.myapp-sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true // пабл ip
  # для того чтобы коннектится по ssh нужно key-pair, который можно создать вручную
  key_name                    = "aws_frankfurt"
  #  key_name                    = aws_key_pair.ssh-key.key_name

  # это как entrypoint script. И тут пишешь bash scripts
  #  user_data = <<EOF
  #    #!/bin/bash
  #    sudo yum update -y && sudo yum install -y docker
  #    sudo systemctl start docker
  #    sudo usermod -aG docker ec2-user
  #    docker run -p 8080:80 nginx
  #  EOF
  user_data = file("entry-script.sh")

  tags = {
    Name : "${var.env_prefix}-server"
  }
}