resource "aws_subnet" "myapp-subnet-1" {
  cidr_block        = var.subnet_cidr_block
  vpc_id            = var.vpc_id
  availability_zone = var.avail_zone
  tags              = {
    Name : "${var.env_prefix}-subnet-1",

  }
}

# Создаем Routes Table, в который по дефолту создастся local table
resource "aws_default_route_table" "myapp_route_table" {
  default_route_table_id = var.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }
  tags = {
    Name : "${var.env_prefix}-rtb"
  }
}

# Создаем Internet Gateway
resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = var.vpc_id
  tags   = {
    Name : "${var.env_prefix}-igw"
  }
}
