provider "aws" {
  region     = "eu-central-1"
  access_key = "AKIAXYAMXO75HA73OO4G"
  secret_key = "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"
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags       = {
    Name : "${var.env_prefix}-vpc",
    vpc_env : "dev"
  }
}
module "myapp-subnet" {
  source                 = "./modules/subnet"
  subnet_cidr_block      = var.subnet_cidr_block
  avail_zone             = var.avail_zone
  env_prefix             = var.env_prefix
  vpc_id                 = aws_vpc.myapp-vpc.id
  default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id
}

module "myapp-webserver" {
  source              = "./modules/webserver"
  avail_zone          = var.avail_zone
  instance_type       = var.instance_type
  public_key_location = var.public_key_location
  env_prefix          = var.env_prefix
  my_ip               = var.my_ip
  subnet_id           = module.myapp-subnet.subnet.id
  vpc_id              = aws_vpc.myapp-vpc.id
  image_name          = var.image_name
}

